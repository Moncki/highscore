<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      /*
        •	Carrera
        •	NickName
      */
      Schema::create('users', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('nickname',16) ->unique();
          $table->string('email',190)   ->unique();
          $table->bigInteger('school_id')->unsigned();
          $table->integer('racha')->unsigned()->default(0);
          $table->integer('totalpoints')->unsigned()->default(0);
          $table->bigInteger('gameracha')->unsigned()->default(0);
          $table->foreign('school_id')->references('id')->on('schools');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
