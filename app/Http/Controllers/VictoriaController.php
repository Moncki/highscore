<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Victoria;
use App\User;

class VictoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function actualizar(Request $request)
    {
        $_request = $request->all();

        if (
          !isset($_request['password'])
        || !isset($_request['nickname'])
        || !isset($_request['juego_id'])
        || !isset($_request['gano'])
        ) return response()->json('Tas miando fuera del perfol',400);

        if (sha1($_request['password']) != sha1('feminismo2'))
          return response()->json('Tas chao',401);

        $user = User::where('nickname','=',$_request['nickname'])->first();
        if (!$user) return response()->json('Usuario no registrado',404);

        $user_id = $user['id'];

        $victoria = DB::table('victorias')
        ->where('user_id',$user_id)
        ->where('juego_id',$_request['juego_id'])
        ->first();
        if (!$victoria) {
          $victoria = DB::table('victorias')->insert([
                       'user_id' => $user_id,
                       'juego_id' => $_request['juego_id'],
                       'victorias' => 0,
                       'racha' => 0,
                  ]);
          $victoria = DB::table('victorias')
          ->where('user_id',$user_id)
          ->where('juego_id',$_request['juego_id'])
          ->first();
          //var_dump($victoria);exit();
        }
        $victoria = Victoria::find($victoria->id);
        if ($_request['gano']) {
          if(!$victoria) return response()->json('Error de Servidor', 500);
            $victoria->victorias = $victoria->victorias + 1;
          if($victoria->victorias > $victoria->racha) {

            $user = User::find($victoria->user_id);
            $user->totalpoints = $user->totalpoints + 1;
            $victoria->racha = $victoria->victorias;

            if ($victoria->racha > $user->racha){
              $user->racha = $victoria->racha;
              $user->gameracha = (integer) $_request['juego_id'];
            }

            $user->save();

          }
        }else{
          if(!$victoria) return response()->json('Error de Servidor', 500);
          $victoria->victorias = 0;
        }
        $victoria->save();
        return response()->json(Victoria::find($victoria->id),200);
    }

}
