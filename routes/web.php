<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('/operator', function () {
    return view('operator');
});

Route::get('/operador', function () {
    return view('operator');
});

Route::get('/register', function () {
    return view('registro');
});
Route::get('/registro', function () {
    return view('registro');
});
