<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JuegosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('juegos')->insert([
           'id' => '1',
           'name' => 'Smash Ultimate',
           'icon' => 'https://vignette.wikia.nocookie.net/fantendo/images/c/c9/SSB2014_-_Super_Smash_Bros..png/revision/latest?cb=20150214033440',
      ]);
      DB::table('juegos')->insert([
           'id' => '2',
           'name' => 'Mortal Kombat',
           'icon' => 'https://portalhoy.com/wp-content/uploads/2015/06/Mortal-Kombat-X.png',
      ]);
      DB::table('juegos')->insert([
           'id' => '3',
           'name' => 'Tekken',
           'icon' => 'http://pngriver.com/wp-content/uploads/2018/03/Download-Tekken-Logo-Transparent-Background-For-Designing-Project.png',
      ]);
      DB::table('juegos')->insert([
           'id' => '4',
           'name' => 'Budokai Tenkaichi 3',
           'icon' => 'https://vignette.wikia.nocookie.net/dragonball/images/f/fd/Dragon_Ball_Z_Budokai_Tenkaichi_3.png/revision/latest/scale-to-width-down/250?cb=20130316172922&path-prefix=es',
      ]);
      DB::table('juegos')->insert([
           'id' => '5',
           'name' => 'Super Smash Bros Brawl',
           'icon' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/SmashBall.svg/1200px-SmashBall.svg.png',
      ]);
      DB::table('juegos')->insert([
           'id' => '6',
           'name' => 'Street Fighter',
           'icon' => 'https://www.freepngimg.com/download/street_fighter/8-2-street-fighter-png-picture.png',
      ]);
      DB::table('juegos')->insert([
           'id' => '7',
           'name' => 'Naruto',
           'icon' => 'https://upload.wikimedia.org/wikipedia/commons/c/c9/Naruto_logo.svg',
      ]);
    }
}
