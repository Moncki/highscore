<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/ranking', 'RankingController@obtener');
Route::post('/victoria', 'VictoriaController@actualizar');
Route::get('/juegos', 'JuegoController@todos');
Route::get('/schools', 'SchoolController@todos');
Route::post('/login', 'Auth\LoginController@login'); //Aun no sive
Route::get('/users', 'UserController@todos');
Route::post('/users', 'Auth\RegisterController@create');
