<?php

namespace App\Http\Controllers;
use App\User;
use App\Victoria;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RankingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function obtener()
    {
      //$victorias = DB::table('victorias');
      $games = DB::table('juegos')->get();
      //$users = DB::table('users');
      $users = User::leftJoin('victorias','victorias.user_id','=','users.id')

      ->orderBy   ('users.racha'        ,         'DESC')
      ->orderBy   ('users.totalpoints'  ,         'DESC')
      ->orderBy   ('victorias.racha'    ,         'DESC')

      //->distinct  ('users.id')
      ->select    ('users.*')
      ->get();

      $usersDistinct = [];

      foreach ($users as $key => $user) {
        $index = null;
        $idSearch = $user['id'];
        //echo '<br>key-'.$key;
        for ($i=0; ($i < $key && $index == null); $i++) {
          //echo '<br>'.$users[$i]['id'].' = '.$idSearch;
          if ($users[$i]['id'] == $idSearch){
            $index = true;
          }
        }
        //echo '<br>x'.$index;

        /*if ($index && $index != -1) {
            var_dump($index);exit();
        }*/

        if ($index == null)
          $usersDistinct[] = $user;
      }
      //var_dump(json_encode($usersDistinct));exit();
      $usersTodo = [];

      for ($i=0; $i < sizeof($usersDistinct); $i++) {
        $usersTodo[$i] = json_decode(json_encode($usersDistinct[$i]),1);
        $usersTodo[$i]['games'] = [];
        $victories = Victoria::where('user_id','=',$usersTodo[$i]['id'])->get();
        foreach ($games as $k => $v) {
          $v =  json_decode(json_encode($v),1);
          $usersTodo[$i]['games'][$v['id']] = 0;
        }
        foreach ($victories as $k => $v) {
          $v =  json_decode(json_encode($v),1);
          $usersTodo[$i]['games'][$v['juego_id']] = (integer) $v['racha'];
        }
      }

      return $usersTodo;
    }

}
