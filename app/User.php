<?php namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'nickname',
        'email',
        'school_id'
    ];

    public static function getUsers(){

      $query = DB::table('users');
      return $query->get();

    }

    public static function createUser(Request $request){

      $_request = $request->all();
      $_request['school_id'] = (integer) $_request['school_id'];

      return User::create([
          'nickname' => $_request['nickname'],
          'email' => $_request['email'],
          'school_id' => $_request['school_id'],
      ]);

    }

}
