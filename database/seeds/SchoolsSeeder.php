<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('schools')->insert([
           'id' => '1',
           'name' => 'Ingenieria Informatica',
      ]);
      DB::table('schools')->insert([
           'id' => '2',
           'name' => 'Ingenieria Civil',
      ]);
      DB::table('schools')->insert([
           'id' => '3',
           'name' => 'Ingenieria Industrial',
      ]);
      DB::table('schools')->insert([
           'id' => '4',
           'name' => 'Comunicacion Social',
      ]);
      DB::table('schools')->insert([
           'id' => '5',
           'name' => 'Derecho',
      ]);
      DB::table('schools')->insert([
           'id' => '6',
           'name' => 'Educacion',
      ]);
      DB::table('schools')->insert([
           'id' => '7',
           'name' => 'Relaciones Industriales',
      ]);
      DB::table('schools')->insert([
           'id' => '8',
           'name' => 'Administracion',
      ]);
      DB::table('schools')->insert([
           'id' => '9',
           'name' => 'Contaduria',
      ]);
      /*DB::table('schools')->insert([
           'id' => '11',
           'name' => 'Otra',
      ]);*/
      DB::table('schools')->insert([
           'id' => '10',
           'name' => 'No soy de la UCAB!',
      ]);


    }
}
