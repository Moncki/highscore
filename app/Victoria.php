<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Victoria extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'juego_id',
        'victorias',
        'racha'
    ];


    public static function getJuegos(){

      $query = DB::table('victorias');
      return $query->get();

    }

}
