<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="url" content="{{ url('/') }}">
        <title>HighScore</title>
        <link href="https://fonts.googleapis.com/css?family=Macondo+Swash+Caps&display=swap" rel="stylesheet">
        <link rel="shortcut icon" href="{{ asset('frontend/favicon.icon') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link media="all" rel="stylesheet" href="{{ asset('frontend/BootstrapMd/css/bootstrap.min.css') }}">
        <link media="all" rel="stylesheet" href="{{ asset('frontend/BootstrapMd/css/mdb.min.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/css/main.css') }}">
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="50">
      <script>
        const baseurl = '{{url("/")}}';
      </script>
      <div id="app" style="min-height: 100vh; min-width: 100vw;">
        <!--<landing></landing>-->
        <records></records>
        <!--<operador></operador>-->
      </div>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
      <script src="{{asset('frontend/Bootstrap/js/bootstrap.min.js')}}"></script>
      <script src="{{asset('frontend/BootstrapMd/js/bootstrap.min.js')}}"></script>
      <script src="{{asset('frontend/BootstrapMd/js/mdb.min.js')}}"></script>
      <script src="{{asset('js/app.js')}}" charset="utf-8"></script>
    </body>
</html>
