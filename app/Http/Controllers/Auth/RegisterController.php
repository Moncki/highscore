<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $_request = $request->all();
        if (
          !isset($_request['school_id'])
        || !isset($_request['nickname'])
        || !isset($_request['email']) ) return response()->json('Debe llenar todos los campos!',400);

        $_request['school_id'] = (integer) $_request['school_id'];

        $listaNegra = [
          'Matson','matson','matsonwashere','Matsonwashere','Pascuale','Arturo','Hermano De Samuel'
        ];
        if (in_array($_request['nickname'],$listaNegra)) return response()->json('Chamo tu que haces aqui?',401);


        if (User::where('nickname','=',$_request['nickname'])->first()) return response()->json('Nombre de usuario en uso!',401);
        if (User::where('email','=',$_request['email'])->first()) return response()->json('Email esta en uso!',401);

        $validator = Validator::make($request->all(), [
          'nickname' => ['required', 'string', 'max:16', 'unique:users'],
          'email' => ['required', 'string', 'email', 'max:190', 'unique:users'],
          'school_id' => ['required', 'numeric','digits_between:1,20'],
        ]);
        if($validator->fails()){
            return response()->json(/*$validator->errors()->toJson()*/'Los campos no son validos', 400);
        }
        $created = User::createUser($request);
        if (!$created) return response()->json('Error de base de datos', 500);

        return $created;//201
    }

}
