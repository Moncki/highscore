<?php

namespace App\Http\Controllers;
use App\Juego;

use Illuminate\Http\Request;

class JuegoController extends Controller
{
    public function todos()
    {
      $vertodos = Juego::getJuegos();
      if (!$vertodos) return response()->json('Error de base de datos', 500);

      return $vertodos;//201
    }

    public function create(Request $request)
    {
      $created = Juego::crearJuego($request);
      if (!$created) return response()->json('Error de base de datos', 500);
      return $created;//201
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
