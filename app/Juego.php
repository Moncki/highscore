<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Juego extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'icon'
    ];


    public static function getJuegos(){

      $query = DB::table('juegos');
      return $query->get();

    }
    public static function crearJuego(Request $request){
      $_request = $request->all();
      $_request['id'] = (integer) $_request['id'];
      return Juego::create([
          'name' => $_request['name'],
          'icon' => $_request['icon'],
      ]);
    }

}
