require('./bootstrap');

window.Vue = require('vue');

Vue.component('landing', require('./components/RegisterComponent.vue').default);
Vue.component('records', require('./components/records.vue').default);
Vue.component('operador', require('./components/operador.vue').default);

const app = new Vue({
    el: '#app',
    mounted(){

    }
});
